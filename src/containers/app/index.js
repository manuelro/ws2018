// @flow
// misc
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Route, Switch } from 'react-router-dom';
import axios from 'axios';
import store from 'store';

// pages
import LoginPage from './pages/Login';
import SignUpPage from './pages/SignUp';
import HomePage from './pages/Home';
// components
import Nav from './components/Nav';
import NotFound from './components/NotFound';

// styles
import './styles/form.css';

type PrivateRoutePropsType = {
  routeProps: Object;
  component: Function;
  location: Object;
  auth: Object;
};
class PrivateRoute extends Component<PrivateRoutePropsType, any>{
  render(){
    const { routeProps, component, location, auth } = this.props;
    const TheComponent = component;
    return <Route {...routeProps} exact render={props => <TheComponent {...props} />}/>;
  }
}

/**
 * checkSession
 * Checks session and performs redirections.
 *
 * @param  {type} auth     The auth state
 * @param  {type} location Location from router
 * @param  {type} history  History from router
 * @return {void}
 */
function checkSession (session, location, history){
  if( !session ){
    if( location.pathname !== '/login' && location.pathname !== '/signup' )
      history.replace('/login');
  }
}

function hashLinkScroll(hash) {
  if (hash !== '') {
    // Push onto callback queue so it runs after the DOM is updated,
    // this is required when navigating from a different page so that
    // the element is rendered on the page before trying to getElementById.
    setTimeout(() => {
      const id = hash.replace('#', '');
      const element = document.getElementById(id);
      element && element.scrollIntoView();
    }, 0);
  }
}

class App extends Component<any> {
  componentWillMount(){
    const { location, history } = this.props;

    // axios interceptor
    axios.interceptors.response.use((response) => {
        return response.data;
    }, function (error) {
        return Promise.reject(error.response.data);
    });

    checkSession(store.get('session'), location, history);
  }

  render() {
    const { auth, location } = this.props;
    const nav = {};

    return (
      <div id="app">
        <Nav data={nav}/>

        <Switch>
          <Route key={1000} exact path='/signup' render={props => <SignUpPage {...props} />}/>
          <Route key={1000} exact path='/login' render={props => <LoginPage {...props} />}/>
          <PrivateRoute
            auth={auth}
            location={location}
            routeProps={{
              path: '/',
            }}
            component={HomePage}
          />
          <Route render={props => <NotFound {...props} />}/>
        </Switch>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  notifications: state.notification.notifications,
  auth: state.auth,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(App);
