// @flow
// misc
import React, { Component } from 'react';
import { withFormsy } from 'formsy-react';

export default function asFormsyInput(WrappedComponent: any){
  class TheComponent extends Component<any, any>{
    changeValue: Function;

    constructor(props){
      super(props);
      this.changeValue = this.changeValue.bind(this);
    }

    changeValue(event){
      this.props.setValue(event.currentTarget.value);
    }

    render(){
      const { props, changeValue } = this;
      const {
        getErrorMessage,
        getErrorMessages,
        getValue,
        hasValue,
        isFormDisabled,
        isValid,
        isPristine,
        isFormSubmitted,
        isRequired,
        isValidValue,
        resetValue,
        setValidations,
        setValue,
        showRequired,
        showError,
        innerRef,
        validationError,
        validationErrors,
        ...restProps
      } = props;

      const value = getValue() || '';

      return (
        <WrappedComponent
          {...restProps}
          onChange={changeValue}
          value={value}
        />
      );
    }
  }

  return withFormsy(TheComponent);
}
