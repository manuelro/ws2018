
// @flow
// ************ HOC ************ //
// Higher Order Component to access Redux State props

import React, { Component } from 'react';
import { connect } from 'react-redux';

const mapStateToProps = (namespaces: Array<string>) => {
  return (state: Object) => {
    if( !namespaces.length ) return { state };

    const newState = {};
    for (let namespace of namespaces) {
      newState[namespace] = state[namespace];
    }

    return newState;
  }
}

const mapDispatchToProps = (dispatch) => {
  return { dispatch };
}

export default function withReduxState(WrappedComponent: any, ...rest: any){
  return connect(mapStateToProps(rest), mapDispatchToProps)(
    class extends Component<any, any> {
      render(){
        return <WrappedComponent {...this.props}/>
      }
    }
  );
}
