// @flow
// misc
import React, { Component } from 'react';
import Formsy, { withFormsy } from 'formsy-react';

export default function asFormsyForm(WrappedComponent: any){
  type Props = {
    handleInvalid?: Function;
    handleInvalidSubmit?: Function;
    handleValid?: Function;
    handleValidSubmit?: Function;
  };

  type State = {
    isValid?: boolean;
  };

  return class TheComponent extends Component<Props, State>{
    state = {
      isValid: true,
    };

    handleInvalid(...rest: any){
      const { props } = this;
      if( props.handleInvalid ) props.handleInvalid.call(this, ...rest);
      this.setState({ isValid: false });
    }

    handleInvalidSubmit(...rest: any){
      const { props } = this;
      if( props.handleInvalidSubmit ) props.handleInvalidSubmit.call(this, ...rest);
      this.setState({ isValid: false });
    }

    handleValid(...rest: any){
      const { props } = this;
      if( props.handleValid ) props.handleValid.call(this, ...rest);
      this.setState({ isValid: true });
    }

    handleValidSubmit(...rest: any){
      const { props } = this;
      if( props.handleValidSubmit ) props.handleValidSubmit.call(this, ...rest);
      this.setState({ isValid: true });
    }

    render(){
      const { props, state } = this;

      return (
        <Formsy
          onInvalid={(...rest) => this.handleInvalid(...rest)}
          onInvalidSubmit={(...rest) => this.handleInvalidSubmit(...rest)}
          onValid={(...rest) => this.handleValid(...rest)}
          onValidSubmit={(...rest) => this.handleValidSubmit(...rest)}
        >
          <WrappedComponent {...props} isValid={state.isValid} />
        </Formsy>
      );
    }
  }
}
