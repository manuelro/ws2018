// @flow

// misc
import React, { Component } from 'react';

// styles
import './text-input/style.css';

type Props = {
  className: string;
};

export default class TextInput extends Component<Props>{
  static defaultProps = {
    className: ''
  };

  render(){
    const { props } = this;
    const { className } = props;

    return <input type='text' {...props} className={`text-input form-control ${className}`} />
  }
}
