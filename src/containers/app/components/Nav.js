// @flow
import React, { Component } from 'react';

// styles
import './nav/style.css';

// components
import Item from './nav/Item';
import Burger from './nav/Burger';
import Logo from './Logo';

type Props = {
  data: Object;
  contentful: Object;
};

export default class Nav extends Component<Props>{
  static defaultProps = {};

  render(){
    const navItems = [];

    return navItems.length ?
      <div className='nav nav--main'>
      <Logo height={40} />

      <ul className='nav--main__items'>
        {navItems} The nav
      </ul>

      <div className='flex--auto'></div>

      <Burger/>

      </div>
      : null
  }
}
