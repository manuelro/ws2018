// @flow
// misc
import React, { Component } from 'react';

// styles
import './message/style.css';

type Props = {
  color: string;
  children: Array<any>;
  className: string;
};

export default class Message extends Component<Props>{
  static defaultProps = {
    color: '',
    className: '',
  };

  render(){
    const { props } = this;
    const { color, className } = props;
    const componentClassName = 'message';
    const colorClassName = color
      ? componentClassName + `--${color.toLowerCase()}` : '';

    return <div className={`${componentClassName} ${colorClassName} ${className}`}>
      {props.children}
    </div>
  }
}
