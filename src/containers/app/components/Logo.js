// @flow
// misc
import React, {Component} from 'react';

// assets
import logo from './logo/assets/logo.png';

type Props = {
  contentful: Object;
  isSecondary?: Boolean;
  width?: Number;
  height?: Number;
  className?: string;
};

export default class Logo extends Component<Props>{
  static defaultProps = {
    contentful: {},
    isSecondary: false,
    width: 120,
    className: 'logo'
  };

  render(){
    const { width, className } = this.props;

    return <img
      alt='Loan Gram'
      width={width}
      src={logo}
      className={className}
      />
  }
}
