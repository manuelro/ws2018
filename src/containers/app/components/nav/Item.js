// @flow

// misc
import React, { Component } from 'react';
import { Link } from 'react-router-dom';

type Props = {
  data: Object;
};

export default class Item extends Component<Props>{
  static defaultProps = {
    data: {
      sys: {},
      fields: {},
    }
  };

  render(){
    const { data } = this.props;
    const { fields } = data;
    return <div className='nav--main__item'>
      <Link to={fields.url}>
        <div className='nav--main__item__name'>{fields.name}</div>
        <div className='nav--main__item__description'>{fields.description}</div>
      </Link>
    </div>;
  }
}
