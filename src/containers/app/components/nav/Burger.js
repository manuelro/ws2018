// @flow
// misc
import React, { Component } from 'react';

// style
import './burger/style.css';

type Props = {
  levels: Number;
};

const Level = props => <div className='burger__level'></div>;

export default class Burger extends Component<Props>{
  static defaultProps = {
    levels: 3,
  };

  render(){
    const { levels } = this.props;
    const levelItems = new Array(levels).fill(0).map((item, index) => <Level key={index} />);

    return <div className='burger'>
      {levelItems}
    </div>;
  }
}
