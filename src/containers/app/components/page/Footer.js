// @flow
// misc
import React, { Component } from 'react';
import ReactMarkdown from 'react-markdown';

// components
import Logo from '../Logo';

// styles
import './footer/style.css';

type Props = {
  contentful: Object;
};

export default class Footer extends Component<Props>{
  static defaultProps = {
    contentful: {}
  };

  render(){
    const { contentful } = this.props;

    const emptyMap = new Map();
    const dataByContentType = contentful.dataByContentType || emptyMap;
    const dataByContentTypeLayout = dataByContentType.get('layout') || emptyMap;
    const layoutEntry = dataByContentTypeLayout.entries().next().value[1] || {};
    const layoutEntryFields = layoutEntry.fields || {};
    const layoutEntryFieldsFooter = layoutEntryFields.footer || {};
    const layoutEntryFieldsFooterSys = layoutEntryFieldsFooter.sys || {};
    const dataByContentTypeBlock = dataByContentType.get('block') || emptyMap;
    const footerEntry = dataByContentTypeBlock.get(layoutEntryFieldsFooterSys.id) || {};
    const footerEntryFields = footerEntry.fields || {};

    return (
      <div className='footer'>
        { footerEntryFields.anchor && <a id={footerEntryFields.anchor}></a> }
        <div className='footer__inner'>
          {
            footerEntry &&
            <div className='footer__section footer__section--info'>
              <ReactMarkdown source={footerEntryFields.body} />
            </div>
          }

          <div className='footer__section footer__section--brand'>
            <div className='footer__section__div'></div>
            <Logo
              width={80}
              contentful={contentful} />
          </div>
        </div>
      </div>
    )
  }
}
