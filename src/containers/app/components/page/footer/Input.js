// @flow

// misc
import React, { Component } from 'react';

// styles
import './input/style.css';

type Props = {};

export default class InputWithIcon extends Component<Props>{
  render(){
    return <div className='input input--icon'>
      <input type='text'
        placeholder='EMAIL' 
        className='flex--auto' />
      <a className='input__button'>Submit</a>
    </div>;
  }
}
