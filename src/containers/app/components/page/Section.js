// @flow
import React, { Component } from 'react';
import Carousel from '@ryo_suga/react-isomorphic-carousel';

// components
import SectionBlock from './section/SectionBlock';
import Logo from '../Logo';

// styles
import './section/style.css';

type Props = {
  data: Object;
  contentful: Object;
  index: Number;
};

export default class Section extends Component<Props>{
  static defaultProps = {
    data: {},
    contentful: {},
    index: 0,
  };

  render(){
    const { data, contentful, index } = this.props;
    const block = contentful.dataById.get(data.sys.id);
    const { fields } = block;

    const blockItems = fields.blocks ? fields.blocks.map((item, index) => <SectionBlock key={index} data={contentful.dataById.get(item.sys.id)} contentful={contentful} />) : [];
    const carouselConfig = { autoSlideInterval: 5000 };
    return <div className={`section section--${index}`}>
      { fields.anchor && <a id={fields.anchor}></a> }
      { !index && <div className="section__logo"><Logo contentful={contentful} width={300} /></div> }
      {
        fields.title || fields.description
        ? <div className='section__contents'>
            { fields.title && <h2 className='section__title'>{fields.title}</h2> }
            { fields.description && <p className='section__description'>{fields.description}</p> }
            { fields.body && <p className='section__body'>{fields.body}</p> }
          </div>
        : null
      }
      {index ? blockItems : <div className='section__carousel'><Carousel {...carouselConfig}>{blockItems}</Carousel></div>}
    </div>
  }
}
