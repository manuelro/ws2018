// @flow
// misc
import React, { Component } from 'react';
import ReactMarkdown from 'react-markdown';

type Props = {
  data: Object;
  contentful: Object;
};

export default class SectionBlock extends Component<Props>{
  static defaultProps = {
    data: {},
    contentful: {
      dataById: new Map(),
    },
  };

  render(){
    const { data, contentful } = this.props;
    const fields = data.fields || {};
    const blocks = fields && fields.blocks ? fields.blocks : [];
    const blockItems = blocks.map((item, index) => {
      const itemData = contentful.dataById.get(item.sys.id);

      return itemData
        ? <SectionBlock key={index} data={itemData} contentful={contentful} />
        : null;
    });

    return <div className='section-block'>
      { fields.anchor && <a id={fields.anchor}></a> }
      {fields.title ? <h4>{fields.title}</h4> : null}
      {fields.description ? <h2>{fields.description}</h2> : null}
      {fields.body ? <ReactMarkdown className='section-block__body' source={fields.body} /> : null}

      {blockItems.length ? <div className='section-block__blocks'>{blockItems}</div> : null}

    </div>
  }
}
