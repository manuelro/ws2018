// @flow

// misc
import React, { Component } from 'react';
import { withRouter } from 'react-router';
import Helmet from 'react-helmet';

// components
import Section from './page/Section';
import Footer from './page/Footer';

// helpers
import env from '../../../../src/helpers/env';

// styles
import './page/style.css';

const defaultTitle = 'LoanGram';
const defaultDescription =
  'Fake App';

const HOST_URL_PRODUCTION = env.getReactVar('HOST_URL_PRODUCTION');
const HOST_URL_DEVELOPMENT = env.getReactVar('HOST_URL_DEVELOPMENT');

const defaultUrl = env.isDevelopment()
  ? HOST_URL_DEVELOPMENT
  : HOST_URL_PRODUCTION;

const defaultImage = `${defaultUrl}/images/logo.jpg`;

const defaultTwitter = '@fake';

const defaultSep = ' | ';

type Props = {
  data: Object;
};

class Page extends Component<Props> {
  static defaultProps: Props = {
    data: {},
  };

  getMetaTags(
    {
      title,
      description,
      image,
      contentType,
      twitter,
      noCrawl,
      published,
      updated,
      category,
      tags
    },
    pathname
  ) {
    const theTitle = title
      ? (title + defaultSep + defaultTitle).substring(0, 60)
      : defaultTitle;
    const theDescription = description
      ? description.substring(0, 155)
      : defaultDescription;
    const theImage = image ? image : defaultImage;

    const metaTags = [
      { itemprop: 'name', content: theTitle },
      { itemprop: 'description', content: theDescription },
      { itemprop: 'image', content: theImage },
      { name: 'description', content: theDescription },
      { name: 'twitter:card', content: 'summary_large_image' },
      { name: 'twitter:site', content: defaultTwitter },
      { name: 'twitter:title', content: theTitle },
      { name: 'twitter:description', content: theDescription },
      { name: 'twitter:creator', content: twitter || defaultTwitter },
      { name: 'twitter:image:src', content: theImage },
      { property: 'og:title', content: theTitle },
      { property: 'og:type', content: contentType || 'website' },
      { property: 'og:url', content: defaultUrl + pathname },
      { property: 'og:image', content: theImage },
      { property: 'og:description', content: theDescription },
      { property: 'og:site_name', content: defaultTitle },
      { property: 'fb:app_id', content: 'xxx' }
    ];

    noCrawl &&
      metaTags.push({ name: 'robots', content: 'noindex, nofollow' });


    published &&
      metaTags.push({ name: 'article:published_time', content: published });

    updated &&
      metaTags.push({ name: 'article:modified_time', content: updated });

    category &&
      metaTags.push({ name: 'article:section', content: category });

    tags &&
      metaTags.push({ name: 'article:tag', content: tags });


    return metaTags;
  }

  render() {
    const { data, contentful, children, id, className, ...rest } = this.props;
    const { fields } = data;

    const pageTitle = fields.title ? fields.title : defaultTitle;
    const pageLocation = defaultUrl + this.props.location.pathname;

    return (
      <div id={fields.slug} className={className}>
        <Helmet
          htmlAttributes={{
            lang: 'en',
            itemscope: undefined,
            itemtype: `http://schema.org/${rest.schema || 'WebPage'}`
          }}
          title={pageTitle}
          link={[
            {
              rel: 'canonical',
              href: pageLocation
            }
          ]}
          meta={this.getMetaTags(rest, this.props.location.pathname)}
        />
        {children}
      </div>
    );
  }
}

export default withRouter(Page);
