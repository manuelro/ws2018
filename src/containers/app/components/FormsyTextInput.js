// @flow
// hoc
import asFormsyInput from '../hoc/asFormsyInput';

// components
import TextInput from './TextInput';

export default asFormsyInput(TextInput);
