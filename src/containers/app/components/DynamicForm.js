// @flow
// misc
import React, { Component } from 'react';
import { Link } from 'react-router-dom';

// components
import Logo from './Logo';
import Button from './Button';
import Message from './Message';
import FormsyTextInput from './FormsyTextInput';

// hoc
import asFormsyForm from '../hoc/asFormsyForm';

// styles
import './dynamic-form/style.css';

type Props = {
  isValid?: boolean;
  fields?: Array<any>;
  submitButtonProps?: Object;
  header?: Object;
  footer?: Object;
  errorState?: Object;
  handleInvalid?: Function;
  handleInvalidSubmit?: Function;
  handleValid?: Function;
  handleValidSubmit?: Function;
};

class DynamicForm extends Component<Props, any>{
  static defaultProps = {
    contentful: {},
    fields: [],
    submitButtonProps: {
      children: 'Submit',
    },
    handleInvalid: () => null,
    handleInvalidSubmit: () => null,
    handleValid: () => null,
    handleValidSubmit: () => null,
  };

  render(){
    const { props } = this;
    const { fields, submitButtonProps, header, footer, errorState } = props;

    const fieldItems = fields.map((item, index) => <item.component key={index} {...item.props} />);

    const Header = header;
    const Footer = footer;

    return (
      <div className='login'>
          <div className='login__form'>
            <div className='login__logo'>
              <Logo
                width={300}
              />
            </div>

            {
              Header
              && <div className='login__form__header'>
                <Header/>
              </div>
            }

            { errorState && <Message color='red' className='login__message'>{errorState.description}</Message> }

            {fieldItems}

            <Button
              disabled={!props.isValid}
              {...submitButtonProps}
              className='login__button'
              type='submit'/>

            {
              Footer
              && <div className='login__related'>
                <Footer/>
              </div>
            }
          </div>
      </div>
    );
  }
}

export default asFormsyForm(DynamicForm);
