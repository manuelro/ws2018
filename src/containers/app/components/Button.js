// @flow

// misc
import React, { Component } from 'react';

// styles
import './button/style.css';

type Props = {
  className: string;
};

export default class TextInput extends Component<Props>{
  static defaultProps = {
    className: ''
  };

  render(){
    const { props } = this;
    const { className } = props;

    return <button {...props} className={`button form-control ${className}`} />
  }
}
