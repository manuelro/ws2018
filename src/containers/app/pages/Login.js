// @flow
// misc
import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import store from 'store';

// components
import FormsyTextInput from '../components/FormsyTextInput';
import DynamicForm from '../components/DynamicForm';

// hoc
import withReduxState from '../hoc/withReduxState';

// modules
import { fetchFromServer, AUTH_LOGOUT } from '../../../modules/auth';

// styles
import './login/style.css';

const submitButtonProps = {
  children: 'Login'
};

const header = props => (<div>Log in to your account</div>);
const footer = props => (
  <div>
    <div className='login__related__section'>
      Don't have an account yet? <Link to='/signup'>Sign Up</Link>
    </div>
  </div>
);

type Props = {
  isValid?: boolean;
  dispatch: Function;
  state?: Object;
};

class Login extends Component<Props, any>{
  static defaultProps = {
    state: {
      auth: {}
    },
  };

  componentWillMount(){
    const { dispatch } = this.props;
    dispatch({ type: AUTH_LOGOUT });
  }

  render(){
    const { props } = this;
    const { dispatch, state } = props;
    const authState = state.auth;
    const signupToken = authState.signupToken || {};

    const fields: Array<any> = [];
    fields.push({
      component: FormsyTextInput,
      props: {
        placeholder: 'Email',
        required: true,
        name: 'username',
        type: 'email',
        validations: 'isEmail',
        value: signupToken.email || '',
      }
    });

    fields.push({
      component: FormsyTextInput,
      props: {
        placeholder: 'Password',
        required: true,
        name: 'password',
        type: 'password',
      }
    });

    return (
      <div className='login'>
        {store.get('session') && <Redirect to='/' />}

        <DynamicForm
          fields={fields}
          submitButtonProps={submitButtonProps}
          header={header}
          footer={footer}
          errorState={authState.loginError}
          handleValidSubmit={data => {
            dispatch(fetchFromServer('login', data));
          }}/>
      </div>
    );
  }
}

export default withReduxState(Login);
