// @flow
// misc
import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import store from 'store';

// components
import FormsyTextInput from '../components/FormsyTextInput';
import DynamicForm from '../components/DynamicForm';

// hoc
import withReduxState from '../hoc/withReduxState';

// modules
import { fetchFromServer } from '../../../modules/auth';

// styles
import './login/style.css';

const fields: Array<any> = [];
fields.push({
  component: FormsyTextInput,
  props: {
    placeholder: 'Email',
    required: true,
    name: 'email',
    type: 'email',
    validations: 'isEmail',
  }
});
fields.push({
  component: FormsyTextInput,
  props: {
    placeholder: 'Password',
    required: true,
    name: 'password',
    type: 'password',
  }
});

const submitButtonProps = {
  children: 'Sign Up'
};

const header = props => (<div>Create a new account</div>);
const footer = props => (
  <div>
    <div className='login__related__section'>
      Have an account already? <Link to='/login'>Login</Link>
    </div>
  </div>
);

type Props = {
  isValid?: boolean;
  dispatch: Function;
  state?: Object;
};

class SignUp extends Component<Props, any>{
  static defaultProps = {
    state: {
      auth: {}
    },
  };

  render(){
    const { props } = this;
    const { dispatch, state } = props;
    const authState = state.auth;

    return (
      <div className='login'>
        {authState.signupToken && <Redirect to='/login' />}

        <DynamicForm
          fields={fields}
          submitButtonProps={submitButtonProps}
          header={header}
          footer={footer}
          errorState={authState.signupError}
          handleValidSubmit={data => {
            dispatch(fetchFromServer('signup', data));
          }}/>
      </div>
    );
  }
}

export default withReduxState(SignUp);
