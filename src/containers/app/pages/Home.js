// @flow
// misc
import React, { Component } from 'react';
import store from 'store';
import { Redirect } from 'react-router-dom';
import { AUTH_LOGOUT } from '../../../modules/auth';

// hoc
import withReduxState from '../hoc/withReduxState';

// components
import Button from '../components/Button';
import AddLoanForm from './home/components/AddLoanForm';

// styles
import './home/style.css';

type Props = {
  dispatch: Function;
  state?: Object;
};

class Home extends Component<Props, any>{
  static defaultProps = {};

  render(){
    const { dispatch } = this.props;
    const session = store.get('session');

    return (
      <div className='home'>
        {!session && <Redirect to='/login' />}

        <div className='home__toolbar'>
          <div className='toolbar_text'>Welcome</div>
          <div className='toolbar_actions'>
            <Button
              className='form-control--small button--yellow'
              onClick={() => dispatch({ type: AUTH_LOGOUT })}
            >
              Log out
            </Button>
          </div>
        </div>

        <AddLoanForm/>

      </div>
    );
  }
}

export default withReduxState(Home);
