// @flow
import React, { Component } from 'react';
import asFormsyForm from '../../../hoc/asFormsyForm';

// styles
import './add-loan-form/style.css';

// components
import FormsyTextInput from '../../../components/FormsyTextInput';
import Button from '../../../components/Button';

type Props = {
  className: string;
};
class AddLoanForm extends Component<Props, any>{
  render(){
    const { className } = this.props;

    return (
      <div className={`add-loan-form ${className}`}>

        <div className='add-loan-form__toolbar'>
          Add loan
          <Button className='form-control--small'>Add</Button>
        </div>

        <FormsyTextInput
          placeholder='Description'
          name='description'
        />
        <div className='add-loan-form__section'>
          <FormsyTextInput
            placeholder='Initial value'
            name='initial_value'
            required
            type='number'
            min={1000000}
            max={10000000}
            value={1000000}
            step={100000}
          />
          <FormsyTextInput
            placeholder='Term (years)'
            name='term'
            required
            type='number'
            min={15}
            max={30}
            value={15}
          />
          <FormsyTextInput
            placeholder='Rate (%)'
            name='rate'
            required
            type='number'
            min={.1}
            max={.5}
            step={.1}
            value={.2}
          />
        </div>
      </div>
    );
  }
}

export default asFormsyForm(AddLoanForm);
