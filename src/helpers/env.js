// @flow


/**
 * Env
 * This helper serves as a gateway to obtain configuration that has been set
 * using Node environment process and the respective emulation on the client side.
 */
export default class Env{
  static getReactVar: Function;
  static getApiWebsite: Function;
  static getEnv: Function;
  static isDevelopment: Function;
  static isProduction: Function;

  /**
   * getReactVar
   * Gets variables that have been set especifically for React during the deployment
   * process using the .env files at the root of the project.
   *
   * @param {type} name The name of the environment variable
   *
   * @return {any} The variable value
   */
  static getReactVar(name: string): any{
    let curatedName = name.toUpperCase();
    return process.env[`REACT_APP_${curatedName}`];
  }

  /**
   * getApiWebsite
   * Get the current API website based on environment configuration.
   *
   * @return {string}
   */
  static getWsEndpoint(): string{
    return this.getReactVar('WS');
  }

  /**
   * getWsAuthEndpoint
   *
   * @return {string}
   */
  static getWsAuthEndpoint(): string{
    return `${this.getWsEndpoint()}/auth`;
  }

  /**
   * getWsApiEndpoint
   *
   * @return {string}
   */
  static getWsApiEndpoint(): string{
    return `${this.getWsEndpoint()}/api`;
  }

  /**
   * getWsAccountEndpoint
   *
   * @return {string}
   */
  static getWsAccountEndpoint(): string{
    return `${this.getWsEndpoint()}/account`;
  }

  /**
   * getEnv
   * Gets the current environment.
   *
   * @return {string}
   */
  static getEnv(): string{
    return String(process.env.NODE_ENV);
  }


  /**
   * isDevelopment
   * Checks if the current environment is development.
   *
   * @return {boolean}
   */
  static isDevelopment(){
    return this.getEnv() === 'development';
  }

  /**
   * isProduction
   * Checks if the current environment is production.
   *
   * @return {boolean}
   */
  static isProduction(){
    return this.getEnv() === 'production';
  }

  /**
   * isTest
   * Checks if the current environment is test.
   *
   * @return {boolean}
   */
  static isTest(){
    return this.getEnv() === 'test';
  }
}
