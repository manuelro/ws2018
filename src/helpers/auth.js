// @flow
// utils
import HTTP from '../utils/http';
// helpers
import Env from './env';

const endpoint: string = Env.getWsAuthEndpoint();
export default class Auth{
  static signup(data: Object, config: Object, cb: Function): Promise<any>{
    return HTTP.post(endpoint, data, config, cb);
  }

  static login(data: Object, config: Object, cb: Function): Promise<any>{
    return HTTP.put(endpoint, data, config, cb);
  }
}
