// @flow
// misc
import store from 'store';

// helpers
import Auth from '../helpers/auth';

export const AUTH_REQUEST = 'LOGIN_REQUEST';
export const AUTH_SUCCESS = 'AUTH_SUCCESS';
export const AUTH_FAILURE = 'AUTH_FAILURE';
export const AUTH_LOGOUT = 'AUTH_LOGOUT';

const initialState = {};

export default (state: Object = initialState, action: Object = {}) => {
  const { type, namespace, payload } = action;

  switch (type) {
    case AUTH_REQUEST:
      return {
        ...state,
        [namespace + 'Error']: null,
      };

    case AUTH_SUCCESS:
      if( namespace === 'login' ) store.set('session', payload);

      return {
        ...state,
        [namespace + 'Error']: null,
        [namespace + 'Token']: payload,
      };

    case AUTH_FAILURE:
      return {
        ...state,
        [namespace + 'Error']: payload,
      };

    case AUTH_LOGOUT:
      store.remove('session');

      return {
        ...state,
        'loginError': null,
        'loginToken': null,
        'sigupError': null,
      };

    default:
      return state;
  }
};

export const fetchFromServer = (
  actionName: string = '',
  data: Object = {},
  config: Object = {
    headers: {},
  },
  cb: Function = () => null,
) => {
  return (dispatch: Function = () => null) => {
    dispatch({ type: AUTH_REQUEST, namespace: actionName });

    const headers = {
      'Content-Type': 'application/json',
      ...config.headers,
    };
    if( Auth[actionName] ) Auth[actionName](
      data,
      {...config, headers},
      (err, res) => {
        if( err ) return dispatch({
          type: AUTH_FAILURE,
          namespace: actionName,
          payload: err
        });

        dispatch({
          type: AUTH_SUCCESS,
          namespace: actionName,
          payload: res
        });
      }
    );
  };
};
