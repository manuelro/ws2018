// @flow
const initialState = {};

export default (state = initialState, action) => {
  const { payload, type } = action;

  switch (type) {
    case DRAWERS_TOGGLE:
      return { ...state, [payload]: !state[payload] };
    case DRAWERS_OPEN:
      return { ...state, [payload]: true };
    case DRAWERS_CLOSE:
      return { ...state, [payload]: false };
    default:
  }

  return state;
}

// module consts
export const DRAWERS_TOGGLE = 'DRAWERS_TOGGLE';
export const DRAWERS_OPEN = 'DRAWERS_OPEN';
export const DRAWERS_CLOSE = 'DRAWERS_CLOSE';


export function toggle(id: string): Object{
  return {
    type: DRAWERS_TOGGLE,
    payload: id,
  }
}

export function open(id: string): Object{
  return {
    type: DRAWERS_OPEN,
    payload: id,
  }
}

export function close(id: string): Object{
  return {
    type: DRAWERS_CLOSE,
    payload: id,
  }
}
