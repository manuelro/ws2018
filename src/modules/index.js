import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import notification from './notification';
import auth from './auth';

export default combineReducers({
  routing: routerReducer,
  notification,
  auth,
});
