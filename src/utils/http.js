// @flow
import axios from 'axios';

function polymorphicDataTransferMethod(
  methodName: string = 'POST',
  url: string = '',
  data: Object = {},
  config: Object = {},
  cb: Function = () => null,
){
  const curatedMethodName = methodName.toLowerCase();
  const methodFromAxios = axios[curatedMethodName]
    ? axios[curatedMethodName] : axios.post;

  return methodFromAxios(url, data, config)
    .then(res => cb(null, res))
    .catch(err => cb(err));
}

export default class HTTP{
  static get(
    url: string = '',
    config: Object = {},
  ): Promise<any>{
    return axios.get(url, config);
  }

  static post(
    url: string = '',
    data: Object = {},
    config: Object = {},
    cb?: Function = () => null,
  ): Promise<any>{
    return polymorphicDataTransferMethod('POST', url, data, config, cb);
  }

  static put(
    url: string = '',
    data: Object = {},
    config: Object = {},
    cb?: Function = () => null,
  ): Promise<any>{
    return polymorphicDataTransferMethod('PUT', url, data, config, cb);
  }

  static patch(
    url: string = '',
    data: Object = {},
    config: Object = {},
    cb?: Function = () => null,
  ): Promise<any>{
    return polymorphicDataTransferMethod('PATCH', url, data, config, cb);
  }
}
