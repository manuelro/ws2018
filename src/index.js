import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { Route } from 'react-router-dom';
import { ConnectedRouter } from 'react-router-redux';
import store, { history } from './store';
import registerServiceWorker from './registerServiceWorker';

// containers
import App from './containers/app';

// styles
import './style.css';

// polyfills
import 'core-js/fn/array/from';
import 'core-js/fn/array/fill';

render(
  <Provider store={store}>
    <ConnectedRouter
      history={history}>
      <Route component={App} />
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root')
);

registerServiceWorker();
