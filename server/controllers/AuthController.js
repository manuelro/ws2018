// misc
import axios from 'axios';
// helpers
import Response from '../helpers/Response';
import Env from '../helpers/Env';

const signupEndpoint = 'https://boxscreencr.auth0.com/dbconnections/signup';
const loginEndpoint = 'https://boxscreencr.auth0.com/oauth/token';

const requestConfigHeaders = {
  'Content-Type': 'application/json'
};

class AuthController{
  /**
   * @static login - allows users to login.
   *
   * headers:
      Content-Type : application/json

   * body:
    {
      "grant_type": "password",
      "username": "mc_ro@msn.com",
      "password": "testpass",
      "scope": "crud:data openid",
      "client_id": "ZWAplA6CuEqmZLz4zu3RpXJKOYXUlgK3",
      "client_secret": "6_zRbqt2XOSneMrm61KGzlu-suFFncbeSgj87zLYW7_xA1tsDArm7CBR83NJ__RR"
    }
   * @return {type}  description
   */
  static login(){
    return async (req, res, next) => {
      let translatedResponse;

      const body = {
        ...req.body,
        scope: 'crud:data openid',
        grant_type: 'password',
        client_id: 'ZWAplA6CuEqmZLz4zu3RpXJKOYXUlgK3',
        client_secret: '6_zRbqt2XOSneMrm61KGzlu-suFFncbeSgj87zLYW7_xA1tsDArm7CBR83NJ__RR',
      };

      try{
        const axiosReponse = await axios({
          method: 'POST',
          url: loginEndpoint,
          headers: requestConfigHeaders,
          data: body
        });

        translatedResponse = Response.translateAxios(axiosReponse);
      } catch(e){
        translatedResponse = Response.translateAxios(e.response);
      }

      res.status(translatedResponse.status).json(translatedResponse.data);
    }
  }


  /**
   * @static signup - allows to sign up new users.
   *
   * headers:
      Content-Type : application/json

   * body:
    {
     "connection": "Username-Password-Authentication",
     "client_id": "ZWAplA6CuEqmZLz4zu3RpXJKOYXUlgK3",
     "email": "mc_ro@msn.com",
     "password": "test"
    }

   * @return {type}  description
   */
  static signup(){
    return async (req, res, next) => {
      let translatedResponse;

      const body = {
        ...req.body,
        connection: 'Username-Password-Authentication',
        client_id: 'ZWAplA6CuEqmZLz4zu3RpXJKOYXUlgK3',
      };

      try{
        const axiosReponse = await axios({
          method: 'POST',
          url: signupEndpoint,
          headers: requestConfigHeaders,
          data: body
        });

        translatedResponse = Response.translateAxios(axiosReponse);
      } catch(e){
        translatedResponse = Response.translateAxios(e.response);
      }

      res.status(translatedResponse.status).json(translatedResponse.data);
    }
  }
}

export default AuthController;
