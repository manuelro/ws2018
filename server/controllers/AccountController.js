// @flow

// misc
import axios from 'axios';
// helpers
import Response from '../helpers/Response';

const recoverRequestEndpoint = 'https://boxscreencr.auth0.com/dbconnections/change_password';

const requestConfigHeaders = {
  'Content-Type': 'application/json'
};

class AccountController{
  /**
   * @static recoverRequest - allow to login existing users.
   *
   * headers:
      Content-Type : application/json

   * body:
    {
      "connection": "Username-Password-Authentication",
      "client_id": "ZWAplA6CuEqmZLz4zu3RpXJKOYXUlgK3",
      "email": "mc_ro@msn.com"
    }
   * @return {type}  description
   */
  static recover(){
    return async (req: Function, res: Function, next: Function) => {
      let translatedResponse;

      try{
        const axiosReponse = await axios({
          method: 'post',
          url: recoverRequestEndpoint,
          headers: requestConfigHeaders,
          data: req.body
        });

        translatedResponse = Response.translateAxios(axiosReponse);
      } catch(e){
        translatedResponse = Response.translateAxios(e.response);
      }

      res.status(translatedResponse.status).json(translatedResponse.data);
    }
  }
}

export default AccountController;
