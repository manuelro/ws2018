import Loan from '../models/Loan';

export default class LoanController {

  static index(req, res, next){
    res.json({ test: 2 });
    // Loan.find({}, (err, data) => res.json(err || data));
  }

  static create(req, res, next){
    const instance = new Loan(req.body);
    instance.save(err => res.json(err || instance));
  }

  static read(req, res, next){
    res.json({ test: 2 });
    // const { id } = req.params;
    // Loan.findById(id, (err, data) => res.json(err || data));
  }

  static update(req, res, next){
    res.json({ test: 2 });
    // const { id } = req.params;
    // const { body } = req;
    //
    // Loan.findOneAndUpdate({ _id: id }, body, { new: true }, (err, data) => res.json(err || data));
  }

}
