// misc
import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const loanSchema = new Schema({
  description: String,
  initial_value: Number,
  current_value: Number,
  currency: String,
  term: Number,
  rate: String,
});

export default mongoose.model('Loan', loanSchema);
