// controllers
import AccountController from '../controllers/AccountController';

import express from 'express';
const router = express.Router();

router.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept'
  );
  next();
});

router.patch('/', AccountController.recover());

export default router;
