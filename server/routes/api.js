// This file includes an optional API common in isomorphic applications

// misc
import express from 'express';
import mongoose from 'mongoose';

// controllers
import LoanController from '../controllers/LoanController';

const router = express.Router();

/*************************************************************
 * Mongo connection
 *************************************************************/
 mongoose.connect('mongodb://wstest:lb7033@ds261429.mlab.com:61429/ws2018');

router.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept'
  );
  next();
});

router.get('/', (req, res, next) => {
  res.json({
    version: 'v1',
    _links: {
      loans: {
        create: {
          verb: 'POST',
          path: '/loans'
        },
        read: {
          verb: 'GET',
          path: '/loans/{:id}'
        },
        update: {
          verb: 'PUT',
          path: '/loans/{:id}'
        }
      },
      'payments': {
        create: {
          verb: 'POST',
          path: '/payments'
        },
        read: {
          verb: 'GET',
          path: '/payments/{:id}'
        },
        update: {
          verb: 'PUT',
          path: '/payments/{:id}'
        }
      }
    }
  });
});

router.get('/loans', LoanController.index);
router.post('/loans', LoanController.create);
router.get('/loans/:id', LoanController.read);
router.put('/loans/:id', LoanController.update);


export default router;
