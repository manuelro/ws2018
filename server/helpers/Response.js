// @flow

type DataType = {
  description: string;
  statusCode?: number;
};

type TranslatedResponse = {
  data: Object;
  status: number;
};

function getSanitizeData(data: Object | string): DataType{
  let sanitizedData : DataType = {
    description: '',
  };

  if( data ){
    if( typeof data === 'string' ){
      sanitizedData.description = data;
    } else {
      sanitizedData = {...data};
      sanitizedData.description = data.error_description || data.description || data.error;
    }
  }
  console.log(data);
  return sanitizedData;
}

export default class Response{
  static translateAxios(axiosResponse: Object = {}): TranslatedResponse{
    const { status, data } = axiosResponse;
    const sanitizedData = getSanitizeData(data);
    sanitizedData.statusCode = status;
    return { data: sanitizedData, status };
  }
}
