// @flow


/**
 * Env
 * This helper serves as a gateway to obtain configuration that has been set
 * using Node environment process and the respective emulation on the client side.
 */
export default class Env{
  static getVar: Function;
  static getEnv: Function;
  static isDevelopment: Function;
  static isProduction: Function;

  /**
   * getVar
   * Gets variables that have been set as env accessible.
   *
   * @param {type} name The name of the environment variable
   *
   * @return {any} The variable value
   */
  static getVar(name: string): any{
    let curatedName = name.toUpperCase();
    return process.env[curatedName];
  }

  /**
   * getEnv
   * Gets the current environment.
   *
   * @return {string}
   */
  static getEnv(): string{
    return String(process.env.NODE_ENV);
  }

  /**
   * isDevelopment
   * Checks if the current environment is development.
   *
   * @return {boolean}
   */
  static isDevelopment(){
    return this.getEnv() === 'development';
  }

  /**
   * isProduction
   * Checks if the current environment is production.
   *
   * @return {boolean}
   */
  static isProduction(){
    return this.getEnv() === 'production';
  }

  /**
   * isTest
   * Checks if the current environment is test.
   *
   * @return {boolean}
   */
  static isTest(){
    return this.getEnv() === 'test';
  }
}
